/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saltcallage.contactclasses;
/**
 *
 * @author mike
 */
public class Contact {

    public Contact(String firstName, String lastName,String cPhone, String cEmail)
    {
        contactName = new Name(firstName, lastName);
        contactPhone = new Phone(cPhone);
        contactEmail = new Email(cEmail);
    }

    private Name contactName;
    private Phone contactPhone;
    private Email contactEmail;
    
    public void UpdateContact(Name newName)
    {
        contactName = newName;
    }
    public void UpdateContact(Email newEmail)
    {
        contactEmail = newEmail;
    }
    public void UpdateContact(Phone newPhone)
    {
        contactPhone = newPhone;
    }
    
    public String[] GetReport()
    {
        String[] ContactData = new String[4];
        ContactData[0] = contactName.GetFirstName();
        ContactData[1] = contactName.GetLastName();
        ContactData[2] = contactPhone.GetValue();
        ContactData[3] = contactEmail.GetValue();
        return ContactData;
    }
    
    public void PrintReport()
    {
        String[] contact = GetReport();
        System.out.println("Contact Name: \t" + contact[0] + contact[1]);
        System.out.println("Contact Phone: \t" + contact[2]);
        System.out.println("Contact Email: \t" + contact[3]);
    }
}
