/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saltcallage.contactclasses;

/**
 *
 * @author 08035703
 */
public class Name extends ContactFields{

    String value2; 
    public Name(String firstName, String surname)
    {
        super(firstName);
        value2 = surname;
    }
    
    @Override
    public void SetValue(String first, String last)
    {
        value = first;
        value2 = last;
    }
    
    public String GetFirstName()
    {
        return value;
    }
    
    public String GetLastName()
    {
        return value2;
    }
    
    public String GetFullName()
    {
        return value + " " + value2;
    }

}